#ifndef __UTILITY_MAP__
#define __UTILITY_MAP__

#include "types.h"
#include "iterator.h"
#include "set.h"

/* Types **********************************************************************/
typedef struct MapType {
	ComparisonFunction key;
	ComparisonFunction value;
} MapType;

typedef struct Map {
	MapType *type;
	Set *keys;
} Map;

typedef struct MapEntry {
	Map *owner;
	void *key;
	void *value;
} MapEntry;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
MapType *map_type_new(ComparisonFunction key, ComparisonFunction value);
MapType *map_type_copy(MapType *type);
MapType *map_by_string(ComparisonFunction value);
int map_type_equals(MapType *type1, MapType *type2);

Map *map_new(MapType *type);
void map_destroy(Map *map);

int map_put(Map *map, void *key, void *value);
void *map_get(Map *map, void *key);
void map_remove(Map *map, char *key);

int map_size(Map *map);
int map_contains(Map *map, void *key);
int map_contains_value(Map *map, void *value);
int map_equals(Map *map1, Map *map2);
Map *map_copy(Map *map);
void map_print(Map *map, PrintingFunction key, PrintingFunction value);

Set *map_key_set(Map *map);
List *map_value_list(Map *map);

int map_compare(void *value, void *target);
Iterator *map_iterator(Map *map);
Iterator *map_query_iterator(Map *map, QueryFunction query, void *arg);
MapEntry *next_map(Iterator *iterator);

int key_compare(void *value, void *target);
int key_compare_exact(void *value, void *target);
int key_compare_value(void *value, void *target);

#endif
