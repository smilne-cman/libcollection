#ifndef __compare__
#define __compare__

/* Includes *******************************************************************/

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
int void_compare(void *value, void *target);
int int_compare(void *value, void *target);
int char_compare(void *value, void *target);
int float_compare(void *value, void *target);
int long_compare(void *value, void *target);
int double_compare(void *value, void *target);
int string_compare(void *value, void *target);

#endif
