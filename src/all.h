#ifndef __libcollection__
#define __libcollection__

/* Includes *******************************************************************/
#include "types.h"
#include "compare.h"
#include "printer.h"
#include "iterator.h"
#include "list.h"
#include "set.h"
#include "map.h"

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/


#endif
