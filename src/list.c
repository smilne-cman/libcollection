#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "compare.h"
#include "printer.h"
#include "iterator.h"
#include "list.h"

/* Prototypes *****************************************************************/
void list_add_managed(List *list, void *value, ListType type);
ListItem *list_index(List *list, int index);
ListItem *new_item(void *value, int managed);
void sort_pass(List *list, ComparisonFunction compare, ListSort direction);
int is_sorted(List *list, ComparisonFunction compare, ListSort direction);
void swap_items(List *list, ListItem *item1, ListItem *item2);
void add_to_list(List *list, ListItem *item);
void free_item(ListItem *item);
void add_typed_value(List *list, void *value);
void set_item_value(ListItem *item, void *value, ListType type);
void set_value_where(List *list, void *target, void *value, ListType type, int (*compare) (void *value, void *arg));
void *list_iterator_next(Iterator *iterator);
int list_iterator_has_next(Iterator *iterator);
int list_query_iterator_has_next(Iterator *iterator);
ComparisonFunction get_comparison(List *list);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
List *list_new(ListType type) {
	List *list = (List *)malloc(sizeof(List));

	list->type = type;
	list->length = 0;
	list->start = NULL;
	list->end = NULL;

	return list;
}

void list_destroy(List *list) {
	while(list->length >= 1) {
		list_remove(list, list->length - 1);
	}

	free(list);
}

/* Setters ********************************************************************/

void list_addi(List *list, int value) {
	int *allocatedValue = (int *)malloc(sizeof(value));
	*allocatedValue = value;

	list_add_managed(list, allocatedValue, ListInt);
}

void list_addc(List *list, char value) {
	char *allocatedValue = (char *)malloc(sizeof(value));
	*allocatedValue = value;

	list_add_managed(list, allocatedValue, ListChar);
}

void list_addf(List *list, float value) {
	float *allocatedValue = (float *)malloc(sizeof(value));
	*allocatedValue = value;

	list_add_managed(list, allocatedValue, ListFloat);
}

void list_addl(List *list, long value) {
	long *allocatedValue = (long *)malloc(sizeof(value));
	*allocatedValue = value;

	list_add_managed(list, allocatedValue, ListLong);
}

void list_addd(List *list, double value) {
	double *allocatedValue = (double *)malloc(sizeof(value));
	*allocatedValue = value;

	list_add_managed(list, allocatedValue, ListDouble);
}

void list_adds(List *list, const char *value) {
	if( list->type != ListAny && list->type != ListString ) return;

	add_to_list(list, new_item(value, FALSE));
}

void list_add_managed(List *list, void *value, ListType type) {
	if( list->type != ListAny && list->type != type ) {
		free(value);
		return;
	}

	add_to_list(list, new_item(value, TRUE));
}

void list_add(List *list, void *value) {
	if( list->type != ListAny ) {
		return;
	}

	add_to_list(list, new_item(value, FALSE));
}

ListItem *new_item(void *value, int managed) {
	ListItem *item = (ListItem *)malloc(sizeof(ListItem));

	item->value = value;
	item->managed = managed;
	item->next = NULL;
	item->previous = NULL;

	return item;
}

void add_to_list(List *list, ListItem *item) {
	list->length++;

	if(list->start == NULL) {
		list->start = item;
		list->end = item;
		return;
	}

	item->previous = list->end;
	list->end->next = item;
	list->end = item;
}

/* Getters ********************************************************************/

void *list_get(List *list, int index) {
  if (list_size(list) > index) {
    return list_index(list, index)->value;
  }

  return NULL;
}

int list_geti(List *list, int index) {
	return *(int *)list_get(list, index);
}

char list_getc(List *list, int index) {
	return *(char *)list_get(list, index);
}

float list_getf(List *list, int index) {
	return *(float *)list_get(list, index);
}

long list_getl(List *list, int index) {
	return *(long *)list_get(list, index);
}

double list_getd(List *list, int index) {
	return *(double *)list_get(list, index);
}

const char *list_gets(List *list, int index) {
	const char *value = (char *)list_get(list, index);
	return value;
}

/* Utility ********************************************************************/

int list_size(List *list) {
	return list->length;
}

void list_insert(List *list, int index, void *value) {
	ListItem *before = list_index(list, index - 1);
	ListItem *after = before->next;

	ListItem *item = new_item(value, FALSE);
	item->next = after;
	item->previous = before;

	before->next = item;
	after->previous = item;

	list->length++;
}

void list_remove(List *list, int index) {
	ListItem *item = list_index(list, index);
	if (item == NULL) return;
  // printf("Deleting item [%i] - %s\n", index, item->managed ? "managed" : "unmanaged");

	ListItem *previous = item->previous;
	ListItem *next = item->next;

	free_item(item);

	list->length--;
	if (previous != NULL && next != NULL) {
		previous->next = next;
    next->previous = previous;
	} else if (previous != NULL) {
		list->end = previous;
		previous->next = NULL;
	} else if (next != NULL) {
		list->start = next;
		next->previous = NULL;
	} else {
    list->start = NULL;
    list->end = NULL;
  }
}

void list_each(List *list, void (*fn)(int i, void *value)) {
	ListItem *item = list->start;

	for(int i = 0; i < list->length; i++) {
		fn(i, item->value);
		item = item->next;
	}
}

List *list_slice(List *list, int start, int end) {
	List *slice = list_new(list->type);

	for(int i = 0; i < list->length; i++) {
		if(i >= start && i <= end) {
			add_typed_value(slice, list_get(list, i));
		}
	}

	return slice;
}

/* Querying *******************************************************************/

ListItem *list_index(List *list, int index) {
	ListItem *item;

	if (index < 0) {

		item = list->end;
		int size = list_size(list);
		for(int i = size - 1; i > size + index; i--) {
			item = item->previous;

			if (item == NULL) return NULL;
		}

	} else {

		item = list->start;
		for(int i = 0; i < index; i++) {
			item = item->next;

			if (item == NULL) return NULL;
		}

	}

	return item;
}

void free_item(ListItem *item) {
	if ( item->managed ) {
		free(item->value);
	}

	free(item);
}

int list_index_of(List *list, void *value) {
	ListItem *item = list->start;

	int index = -1;
	for(int i = 0; i < list->length; i++) {
		if(value == item->value) {
			index = i;
			break;
		}

		item = item->next;
	}

	return index;
}

int list_index_ofi(List *list, int value) {
	return list_index_where(list, &value, int_compare);
}

int list_index_ofc(List *list, char value) {
	return list_index_where(list, &value, char_compare);
}

int list_index_off(List *list, float value) {
	return list_index_where(list, &value, float_compare);
}

int list_index_ofl(List *list, long value) {
	return list_index_where(list, &value, long_compare);
}

int list_index_ofd(List *list, double value) {
	return list_index_where(list, &value, double_compare);
}

int list_index_ofs(List *list, const char *value) {
	return list_index_where(list, value, string_compare);
}

int list_index_where(List *list, void *arg, ComparisonFunction compare) {
	ListItem *item = list->start;

	int index = -1;
	for(int i = 0; i < list->length; i++) {
		if(compare(item->value, arg) == EQUAL) {
			index = i;
			break;
		}

		item = item->next;
	}

	return index;
}

void *list_value_where(List *list, void *arg, ComparisonFunction compare) {
	ListItem *item = list->start;

	void *value = NULL;
	for(int i = 0; i < list->length; i++) {
		if(compare(item->value, arg) == 0) {
			value = item->value;
			break;
		}

		item = item->next;
	}

	return value;
}

const char *list_join(List *list, const char *separator, int length) {
  char *result = (char *)malloc(sizeof(char) * length);
  sprintf(result, "");

  if (list->type != ListString) {
    return result;
  }

  int first = TRUE;
	Iterator *iterator = list_iterator(list);
	while(has_next(iterator)) {
		const char *item = (const char *)next(iterator);

		if (first) {
			sprintf(result, "%s", item);
			first = FALSE;
		} else {
			sprintf(result, "%s%s%s", result, separator, item);
		}
	}

	iterator_destroy(iterator);

  return result;
}

/* Printing *******************************************************************/

void list_print(List *list, PrintingFunction printer) {
  printf("[ ");
  for(int i = 0; i < list->length; i++) {
    if(i > 0) printf(", ");
    printer(list_get(list, i));
  }
  printf(" ]\n");
}

void list_printi(List *list) {
  list_print(list, int_printer);
}

void list_printc(List *list) {
  list_print(list, char_printer);
}

void list_printf(List *list) {
	list_print(list, float_printer);
}

void list_printl(List *list) {
	list_print(list, long_printer);
}

void list_printd(List *list) {
	list_print(list, double_printer);
}

void list_prints(List *list) {
	list_print(list, string_printer);
}

/* Order By *******************************************************************/

void list_order_by(List *list, ComparisonFunction compare, ListSort direction) {
  while(!is_sorted(list, compare, direction)) {
    sort_pass(list, compare, direction);
  }
}

void sort_pass(List *list, ComparisonFunction compare, ListSort direction) {
  ListItem *item = list->end;
  for(int i = 0; list->length; i++) {
    if(item->previous == NULL) {
      return;
    }

    if(
      (direction == SortAscending && compare(item->value, item->previous->value) < 0) ||
      (direction == SortDescending && compare(item->value, item->previous->value) > 0)) {
      swap_items(list, item, item->previous);
    }else {
      item = item->previous;
    }
  }
}

int is_sorted(List *list, ComparisonFunction compare, ListSort direction) {
  ListItem *item = list->end;
  for(int i = 0; i < list->length; i++) {
    if(item->previous == NULL) {
      return TRUE;
    }

    if(direction == SortAscending && compare(item->value, item->previous->value) < 0) {
      return FALSE;
    } else if(direction == SortDescending && compare(item->value, item->previous->value) > 0) {
      return FALSE;
    }

    item = item->previous;
  }

  return TRUE;
}

void swap_items(List *list, ListItem *item1, ListItem *item2) {
  if(item1 == NULL || item2 == NULL) {
    return;
  }

  ListItem *item1previous = item1->previous;
  ListItem *item1next = item1->next;
  ListItem *item2previous = item2->previous;
  ListItem *item2next = item2->next;

  if(list->start == item1) {
    list->start = item2;
  } else if(list->start == item2) {
    list->start = item1;
  }

  if(list->end == item1) {
    list->end = item2;
  } else if(list->end == item2) {
    list->end = item1;
  }

  if(item1->next != NULL && item1->next != item2) {
    item1->next->previous = item2;
  }

  if(item1->previous != NULL && item1->previous != item2) {
    item1->previous->next = item2;
  }

  if(item2->next != NULL && item2->next != item1) {
    item2->next->previous = item1;
  }

  if(item2->previous != NULL && item2->previous != item1) {
    item2->previous->next = item1;
  }

  item1->next = item2next == item1 ? item2 : item2next;
  item1->previous = item2previous == item1 ? item2 : item2previous;
  item2->next = item1next == item2 ? item1 : item1next;
  item2->previous = item1previous == item2 ? item1 : item1previous;
}

List *list_copy(List *list) {
	List *copy = list_new(list->type);

	for(int i = 0; i < list->length; i++) {
		add_typed_value(copy, list_get(list, i));
	}

	return copy;
}

int list_equals(List *expected, List *actual, ComparisonFunction compare) {
  if(expected->type != actual->type) return FALSE;
  if(expected->length != actual->length) return FALSE;

  ListItem *currentExpected = expected->start;
  ListItem *currentActual = actual->start;
  for(int i = 0; i < expected->length; i++) {
    if(compare(currentExpected->value, currentActual->value) != 0) return FALSE;

    currentExpected = currentExpected->next;
    currentActual = currentActual->next;
  }

  return TRUE;
}

int list_contains(List *list, void *value, ComparisonFunction compare) {
  ListItem *current = list->start;
  for(int i = 0; i < list->length; i++) {
    if(compare(current->value, value) == EQUAL) return TRUE;

    current = current->next;
  }

  return FALSE;
}

void list_append(List *target, List *source) {
  if (target->type != ListAny && target->type != source->type) return;

  ListItem *current = source->start;
  for(int i = 0; i < source->length; i++) {
    add_typed_value(target, current->value);

    current = current->next;
  }
}

Iterator *list_iterator(List *list) {
	return iterator_new(list, list_iterator_next, list_iterator_has_next, list->start);
}

typedef struct ListQueryParameters {
	ComparisonFunction query;
	ListItem *current;
	void *argument;
	int queried;
} ListQueryParameters;

void *list_query_iterator_next(Iterator *iterator) {
	ListQueryParameters *params = (ListQueryParameters *)iterator->meta;

  if(params->queried) {
    params->queried = FALSE;
    return params->current == NULL ? NULL : params->current->value;
  }

	return has_next(iterator) ? params->current->value : NULL;
}

int list_query_iterator_has_next(Iterator *iterator) {
	ListQueryParameters *params = (ListQueryParameters *)iterator->meta;

	ListItem *current = params->current->next;
	while(current != NULL && params->query(current->value, params->argument) == FALSE) {
		current = current->next;
	}

	if(current == NULL) {
		free(iterator->meta);
		iterator->meta = NULL;

		return FALSE;
	}

	params->current = current;
	params->queried = TRUE;
	return TRUE;
}

Iterator *list_query_iterator(List *list, QueryFunction query, void *arg) {
	ListQueryParameters *params = (ListQueryParameters *)malloc(sizeof(ListQueryParameters));

	params->query = query;
	params->current = list->start;
	params->argument = arg;
	params->queried = FALSE;

	return iterator_new(list, list_query_iterator_next, list_query_iterator_has_next, params);
}

void *list_iterator_next(Iterator *iterator) {
	ListItem *current = (ListItem *)iterator->meta;
	iterator->meta = current->next;

	return current->value;
}

int list_iterator_has_next(Iterator *iterator) {
	// Relying on next() to set next list item to meta which if end of list will
	// be null, making the iterator invalid so it always returns false.
	return TRUE;
}

int list_compare(void *value, void *target) {
	List *list1 = (List *)value;
	List *list2 = (List *)target;

	if(list1->type != list2->type) return FALSE;

	return list_equals(list1, list2, get_comparison(list1));
}

ComparisonFunction get_comparison(List *list) {
	switch(list->type) {
    case ListAny:
      return void_compare;
    case ListInt:
      return int_compare;
    case ListChar:
      return char_compare;
    case ListFloat:
      return float_compare;
    case ListLong:
      return long_compare;
    case ListDouble:
      return double_compare;
		case ListString:
			return string_compare;
  }
}

void add_typed_value(List *list, void *value) {
  switch(list->type) {
    case ListAny:
      list_add(list, value);
      break;
    case ListInt:
      list_addi(list, *(int *)value);
      break;
    case ListChar:
      list_addc(list, *(char *)value);
      break;
    case ListFloat:
      list_addf(list, *(float *)value);
      break;
    case ListLong:
      list_addl(list, *(long *)value);
      break;
    case ListDouble:
      list_addd(list, *(double *)value);
			break;
		case ListString:
			list_adds(list, (char *)value);
      break;
  }
}
