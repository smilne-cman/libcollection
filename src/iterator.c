#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "iterator.h"

/* Prototypes *****************************************************************/
static int is_valid(Iterator *iterator);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Iterator *iterator_new(void *collection, void *(next)(Iterator *iterator), int (*hasNext)(struct Iterator *iterator), void *meta) {
  Iterator *iterator = (Iterator *)malloc(sizeof(Iterator));

  iterator->collection = collection;
  iterator->next = next;
  iterator->hasNext = hasNext;
  iterator->meta = meta;

  return iterator;
}

void iterator_destroy(Iterator *iterator) {
  free(iterator);
}

void *next(Iterator *iterator) {
  if(!is_valid(iterator)) return NULL;

  return iterator->next(iterator);
}

int has_next(Iterator *iterator) {
  if(!is_valid(iterator)) return FALSE;

  return iterator->hasNext(iterator);
}

static int is_valid(Iterator *iterator) {
  return iterator->next != NULL
    && iterator->hasNext != NULL
    && iterator->meta != NULL;
}
