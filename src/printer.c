#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "printer.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void int_printer(void *value) { printf("%i", *(int *)value); }
void char_printer(void *value) { printf("'%s'", (char *)value); }
void float_printer(void *value) { printf("%f", *( float*)value); }
void long_printer(void *value) { printf("%li", *( long*)value); }
void double_printer(void *value) { printf("%g", *( double*)value); }
void string_printer(void *value) { printf("%s", ( char*)value); }
