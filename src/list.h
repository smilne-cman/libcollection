#ifndef __UTILITY_LIST__
#define __UTILITY_LIST__

#include "types.h"
#include "iterator.h"

/* Types **********************************************************************/
typedef enum ListType { ListAny, ListInt, ListChar, ListFloat, ListLong, ListDouble, ListString } ListType;
typedef enum ListSort { SortAscending, SortDescending } ListSort;

typedef struct ListItem {
	void *value;
	int managed;
	struct ListItem *next;
	struct ListItem *previous;
} ListItem;

typedef struct List {
	ListType type;
	int length;
	ListItem *start;
	ListItem *end;
} List;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
List *list_new(ListType type);
void list_destroy(List *list);

void list_add(List *list, void *value);
void list_addi(List *list, int value);
void list_addc(List *list, char value);
void list_addf(List *list, float value);
void list_addl(List *list, long value);
void list_addd(List *list, double value);
void list_adds(List *list, const char *value);

void *list_get(List *list, int index);
int list_geti(List *list, int index);
char list_getc(List *list, int index);
float list_getf(List *list, int index);
long list_getl(List *list, int index);
double list_getd(List *list, int index);
const char *list_gets(List *list, int index);

int list_size(List *list);
void list_insert(List *list, int index, void *value);
void list_remove(List *list, int index);
List *list_slice(List *list, int from, int to);

void list_each(List *list, void (*fn)(int i, void *value));

int list_index_of(List *list, void *value);
int list_index_ofi(List *list, int value);
int list_index_ofc(List *list, char value);
int list_index_off(List *list, float value);
int list_index_ofl(List *list, long value);
int list_index_ofd(List *list, double value);
int list_index_ofs(List *list, const char *value);

int list_index_where(List *list, void *arg, ComparisonFunction compare);
void *list_value_where(List *list, void *arg, ComparisonFunction compare);

const char *list_join(List *list, const char *separator, int length);

void list_print(List *list, PrintingFunction printer);
void list_printi(List *list);
void list_printc(List *list);
void list_printf(List *list);
void list_printl(List *list);
void list_printd(List *list);
void list_prints(List *list);

void list_order_by(List *list, ComparisonFunction compare, ListSort direction);
List *list_copy(List *list);

int list_equals(List *expected, List *actual, ComparisonFunction compare);
int list_contains(List *list, void *value, ComparisonFunction compare);
void list_append(List *target, List *source);

int list_compare(void *value, void *target);
Iterator *list_iterator(List *list);
Iterator *list_query_iterator(List *list, QueryFunction query, void *arg);

#endif
