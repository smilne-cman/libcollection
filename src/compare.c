#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "compare.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
int void_compare(void *value, void *target) {
  if(value == target) {
    return EQUAL;
  }

  return LESS;
}

int int_compare(void *value, void *target) {
  if(value == NULL || target == NULL) {
    return LESS;
  }

  int intValue = *(int *)value;
  int intTarget = *(int *)target;

  if(intValue > intTarget) {
    return GREATER;
  }

  if(intValue == intTarget) {
    return EQUAL;
  }

  return LESS;
}

int char_compare(void *value, void *target) {
  if(value == NULL || target == NULL) {
    return LESS;
  }

  char charValue = *(char *)value;
  char charTarget = *(char *)target;

  if(charValue > charTarget) {
    return GREATER;
  }

  if(charValue == charTarget) {
    return EQUAL;
  }

  return LESS;
}

int float_compare(void *value, void *target) {
  if(value == NULL || target == NULL) {
    return LESS;
  }

  float floatValue = *(float *)value;
  float floatTarget = *(float *)target;

  if(floatValue > floatTarget) {
    return GREATER;
  }

  if(floatValue == floatTarget) {
    return EQUAL;
  }

  return LESS;
}

int long_compare(void *value, void *target) {
  if(value == NULL || target == NULL) {
    return LESS;
  }

  long longValue = *(long *)value;
  long longTarget = *(long *)target;

  if(longValue > longTarget) {
    return GREATER;
  }

  if(longValue == longTarget) {
    return EQUAL;
  }

  return LESS;
}

int double_compare(void *value, void *target) {
  if(value == NULL || target == NULL) {
    return LESS;
  }

  double doubleValue = *(double *)value;
  double doubleTarget = *(double *)target;

  if(doubleValue > doubleTarget) {
    return GREATER;
  }

  if(doubleValue == doubleTarget) {
    return EQUAL;
  }

  return LESS;
}

int string_compare(void *value, void *target) {
  const char *stringValue = (char *)value;
  const char *stringTarget = (char *)target;

  return strcmp(stringValue, stringTarget);
}
