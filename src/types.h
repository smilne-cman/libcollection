#ifndef __UTILITY_TYPES__
#define __UTILITY_TYPES__

/* Includes *******************************************************************/

/* Types **********************************************************************/
typedef int (*ComparisonFunction)(void *value, void *target);
typedef int (*QueryFunction)(void *value, void *arg);
typedef void (*PrintingFunction)(void *value);

/* Macros *********************************************************************/
#define FALSE 0
#define TRUE 1
#define EQUAL 0
#define GREATER 1
#define LESS -1

/* Global Functions ***********************************************************/

#endif
