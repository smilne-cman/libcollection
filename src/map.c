#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "compare.h"
#include "map.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
MapEntry *new_map_entry(Map *owner, void *key, void* value);
MapEntry *new_entry_wrapper(void *key);
MapEntry *new_entry_wrapper_value(void *value);
void *map_iterator_next(Iterator *iterator);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
MapType *map_type_new(ComparisonFunction key, ComparisonFunction value) {
	MapType *type = (MapType *)malloc(sizeof(MapType));

	type->key = key;
	type->value = value;

	return type;
}

MapType *map_type_copy(MapType *type) {
	MapType *copy = (MapType *)malloc(sizeof(MapType));

	copy->key = type->key;
	copy->value = type->value;

	return copy;
}

MapType *map_by_string(ComparisonFunction value) {
	return map_type_new(string_compare, value);
}

int map_type_equals(MapType *type1, MapType *type2) {
	if(type1->key != type2->key) return FALSE;
	if(type1->value != type2->value) return FALSE;

	return TRUE;
}

Map *map_new(MapType *type) {
	Map *map = (Map *)malloc(sizeof(Map));

	map->type = type;
	map->keys = set_new(key_compare);

	return map;
}

void map_destroy(Map *map) {
	Iterator *iterator = set_iterator(map->keys);
	while(has_next(iterator)) {
		free(next(iterator));
	}

	iterator_destroy(iterator);
	set_destroy(map->keys);
	free(map->type);
	free(map);
}

int map_put(Map *map, void *key, void *value) {
	Iterator *iterator = set_iterator(map->keys);

	while(has_next(iterator)) {
		MapEntry *entry = (MapEntry *)next(iterator);

		if(map->type->key(entry->key, key) == EQUAL) {
			entry->value = value;
			iterator_destroy(iterator);
			return FALSE;
		}
	}

	iterator_destroy(iterator);
	MapEntry *entry = new_map_entry(map, key, value);
	return set_add(map->keys, entry);
}

void *map_get(Map *map, void *key) {
	Iterator *iterator = set_iterator(map->keys);

	while(has_next(iterator)) {
		MapEntry *entry = (MapEntry *)next(iterator);

		if(map->type->key(entry->key, key) == EQUAL) {
			iterator_destroy(iterator);
			return entry->value;
		}
	}

	iterator_destroy(iterator);
	return NULL;
}

void map_remove(Map *map, char *key) {
	MapEntry *wrapper = new_entry_wrapper(key);

	set_remove(map->keys, wrapper);

	free(wrapper);
}

int map_size(Map *map) {
	return set_size(map->keys);
}

int map_contains(Map *map, void *key) {
	MapEntry *wrapper = new_entry_wrapper(key);

	int contains = set_contains(map->keys, wrapper);

	free(wrapper);
	return contains;
}

int map_contains_value(Map *map, void *value) {
	MapEntry *wrapper = new_entry_wrapper_value(value);

	map->keys->compare = key_compare_value;
	int contains = set_contains(map->keys, wrapper);
	map->keys->compare = key_compare;

	free(wrapper);
	return contains;
}

int map_equals(Map *map1, Map *map2) {
	if(map_type_equals(map1->type, map2->type) == FALSE) return FALSE;

	map1->keys->compare = key_compare_exact;
	map2->keys->compare = key_compare_exact;
	int equals = set_equals(map1->keys, map2->keys);
	map1->keys->compare = key_compare;
	map2->keys->compare = key_compare;

	return equals;
}

Map *map_copy(Map *map) {
	Map *copy = map_new(map_type_copy(map->type));

	Iterator *iterator = map_iterator(map);
	while(has_next(iterator)) {
		MapEntry *entry = (MapEntry *)next(iterator);

		map_put(copy, entry->key, entry->value);
	}

	iterator_destroy(iterator);
	return copy;
}

Set *map_key_set(Map *map) {
	Set *set = set_new(map->type->key);

	Iterator *iterator = map_iterator(map);
	while(has_next(iterator)) {
		set_managed_add(set, next_map(iterator)->key);
	}

	iterator_destroy(iterator);
	return set;
}

List *map_value_list(Map *map) {
	List *list = list_new(ListAny);

	Iterator *iterator = map_iterator(map);
	while(has_next(iterator)) {
		list_add(list, next_map(iterator)->value);
	}

	iterator_destroy(iterator);
	return list;
}

int map_compare(void *value, void *target) {
	Map *map1 = (Map *)value;
	Map *map2 = (Map *)target;

	return map_equals(map1, map2);
}

Iterator *map_iterator(Map *map) {
	return set_iterator(map->keys);
}

Iterator *map_query_iterator(Map *map, QueryFunction query, void *arg) {
	return set_query_iterator(map->keys, query, arg);
}

MapEntry *next_map(Iterator *iterator) {
	return (MapEntry *)next(iterator);
}

void map_print(Map *map, PrintingFunction key, PrintingFunction value) {
	int first = TRUE;
	printf("{ ");

	Iterator *iterator = map_iterator(map);
	while(has_next(iterator)) {
		MapEntry *entry = next_map(iterator);

		if(first) {
			first = FALSE;
		} else {
			printf(", ");
		}

		key(entry->key);
		printf(" => ");
		value(entry->value);
	}

	iterator_destroy(iterator);
  printf(" }\n");
}

int key_compare(void *value, void *target) {
	if(value == NULL || target == NULL) return LESS;

  MapEntry *entry1 = (MapEntry *)value;
	MapEntry *entry2 = (MapEntry *)target;

  Map *map = entry1->owner;

	return map->type->key(entry1->key, entry2->key);
}

int key_compare_exact(void *value, void *target) {
	if(value == NULL || target == NULL) return LESS;

  MapEntry *entry1 = (MapEntry *)value;
	MapEntry *entry2 = (MapEntry *)target;

  Map *map = entry1->owner;

	if(map->type->key(entry1->key, entry2->key) != EQUAL) return LESS;
	if(map->type->value(entry1->value, entry2->value)) return LESS;

	return EQUAL;
}

int key_compare_value(void *value, void *target) {
	if(value == NULL || target == NULL) return LESS;

  MapEntry *entry1 = (MapEntry *)value;
	MapEntry *entry2 = (MapEntry *)target;

  Map *map = entry1->owner;

	return map->type->value(entry1->value, entry2->value);
}

MapEntry *new_map_entry(Map *owner, void *key, void* value) {
	MapEntry *entry = (MapEntry *)malloc(sizeof(MapEntry));

  entry->owner = owner;
	entry->value = value;

  if (owner != NULL && owner->type->key == string_compare) {
    char *keyClone = (char *)malloc(sizeof(char) * strlen(key));
    strcpy(keyClone, key);

    entry->key = keyClone;
  } else {
    entry->key = key;
  }

	return entry;
}

MapEntry *new_entry_wrapper(void *key) {
	return new_map_entry(NULL, key, NULL);
}

MapEntry *new_entry_wrapper_value(void *value) {
	return new_map_entry(NULL, "", value);
}
