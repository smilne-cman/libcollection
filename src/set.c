#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "compare.h"
#include "set.h"

/* Prototypes *****************************************************************/
Set *set_new_type(ListType type, ComparisonFunction compare);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Set *set_new(ComparisonFunction compare) {
  if(compare == int_compare) return set_newi();
  if(compare == char_compare) return set_newc();
  if(compare == float_compare) return set_newf();
  if(compare == long_compare) return set_newl();
  if(compare == double_compare) return set_newd();
  if(compare == string_compare) return set_news();

  return set_new_type(ListAny, compare);
}

Set *set_newi() {
  return set_new_type(ListInt, int_compare);
}

Set *set_newc() {
  return set_new_type(ListChar, char_compare);
}

Set *set_newf() {
  return set_new_type(ListFloat, float_compare);
}

Set *set_newl() {
  return set_new_type(ListLong, long_compare);
}

Set *set_newd() {
  return set_new_type(ListDouble, double_compare);
}

Set *set_news() {
  return set_new_type(ListString, string_compare);
}

Set *set_new_type(ListType type, ComparisonFunction compare) {
  Set *set = (Set *)malloc(sizeof(Set));

  set->list = list_new(type);
  set->compare = compare;

	return set;
}

Set *set_from_list(List *list, ComparisonFunction compare) {
  Set *set = set_new(compare);

  Iterator *iterator = list_iterator(list);
  while(has_next(iterator)) {
    set_managed_add(set, next(iterator));
  }

  return set;
}

void set_destroy(Set *set) {
	list_destroy(set->list);
	free(set);
}

/* Setters ********************************************************************/

int set_add(Set *set, void *value) {
  if(set_contains(set, value)) return FALSE;

  list_add(set->list, value);
  return TRUE;
}

int set_addi(Set *set, int value) {
  if(set_containsi(set, value)) return FALSE;

  list_addi(set->list, value);
  return TRUE;
}

int set_addc(Set *set, char value) {
  if(set_containsc(set, value)) return FALSE;

  list_addc(set->list, value);
  return TRUE;
}

int set_addf(Set *set, float value) {
  if(set_containsf(set, value)) return FALSE;

  list_addf(set->list, value);
  return TRUE;
}

int set_addl(Set *set, long value) {
  if(set_containsl(set, value)) return FALSE;

  list_addl(set->list, value);
  return TRUE;
}

int set_addd(Set *set, double value) {
  if(set_containsd(set, value)) return FALSE;

  list_addd(set->list, value);
  return TRUE;
}

int set_adds(Set *set, const char *value) {
  if(set_containss(set, value)) return FALSE;

  list_adds(set->list, value);
  return TRUE;
}

int set_managed_add(Set *set, void *value) {
  switch(set->list->type) {
    case ListAny:
      return set_add(set, value);
    case ListInt:
      return set_addi(set, *(int *)value);
    case ListChar:
      return set_addc(set, *(char *)value);
    case ListFloat:
      return set_addf(set, *(float *)value);
    case ListLong:
      return set_addl(set, *(long *)value);
    case ListDouble:
      return set_addd(set, *(double *)value);
    case ListString:
      return set_adds(set, (char *)value);
    default:
      return FALSE;
  }
}

/* Removers *******************************************************************/

void set_remove(Set *set, void *value) {
  if(!set_contains(set, value)) return;

  list_remove(set->list, list_index_where(set->list, value, set->compare));
}

void set_removei(Set *set, int value) {
  if(!set_containsi(set, value)) return;

  list_remove(set->list, list_index_ofi(set->list, value));
}

void set_removec(Set *set, char value) {
  if(!set_containsc(set, value)) return;

  list_remove(set->list, list_index_ofc(set->list, value));
}

void set_removef(Set *set, float value) {
  if(!set_containsf(set, value)) return;

  list_remove(set->list, list_index_off(set->list, value));
}

void set_removel(Set *set, long value) {
  if(!set_containsl(set, value)) return;

  list_remove(set->list, list_index_ofl(set->list, value));
}

void set_removed(Set *set, double value) {
  if(!set_containsd(set, value)) return;

  list_remove(set->list, list_index_ofd(set->list, value));
}

void set_removes(Set *set, const char *value) {
  if(!set_containss(set, value)) return;

  list_remove(set->list, list_index_ofs(set->list, value));
}

/* Contains *******************************************************************/

int set_contains(Set *set, void *value) {
  return list_contains(set->list, value, set->compare);
}

int set_containsi(Set *set, int value) {
  return set_contains(set, &value);
}

int set_containsc(Set *set, char value) {
  return set_contains(set, &value);
}

int set_containsf(Set *set, float value) {
  return set_contains(set, &value);
}

int set_containsl(Set *set, long value) {
  return set_contains(set, &value);
}

int set_containsd(Set *set, double value) {
  return set_contains(set, &value);
}

int set_containss(Set *set, const char *value) {
  return set_contains(set, value);
}

/* Misc ***********************************************************************/

int set_size(Set *set) {
  return set->list->length;
}

void set_each(Set *set, void (*fn)(void *value)) {
  Iterator *iterator = list_iterator(set->list);

  while(has_next(iterator)) {
    fn( next(iterator) );
  }

  iterator_destroy(iterator);
}

int set_equals(Set *set1, Set *set2) {
  if(set1->compare != set2->compare) return FALSE;
  if(set_size(set1) != set_size(set2)) return FALSE;

  Iterator *iterator1 = list_iterator(set1->list);
  Iterator *iterator2 = list_iterator(set2->list);
  while(has_next(iterator1)) {
    void *value = next(iterator1);
    void *otherValue = next(iterator2);

    if(set_contains(set2, value) == FALSE || set_contains(set1, otherValue) == FALSE) {
      iterator_destroy(iterator1);
      iterator_destroy(iterator2);
      return FALSE;
    }
  }

  iterator_destroy(iterator1);
  iterator_destroy(iterator2);
  return TRUE;
}

Set *set_copy(Set* set) {
  Iterator *iterator = list_iterator(set->list);
  Set *copy = set_new(set->compare);

  while(has_next(iterator)) {
    set_managed_add(copy, next(iterator));
  }

  iterator_destroy(iterator);
  return copy;
}

Set *set_merge(Set *set, Set *other) {
  Set *merged = set->compare == other->compare ? set_new(set->compare) : set_new(void_compare);
  Iterator *iterator = list_iterator(set->list);
  Iterator *otherIterator = list_iterator(other->list);

  while(has_next(iterator)) {
    set_managed_add(merged, next(iterator));
  }

  while(has_next(otherIterator)) {
    set_managed_add(merged, next(otherIterator));
  }

  iterator_destroy(iterator);
  iterator_destroy(otherIterator);
  return merged;
}

Set *set_common(Set *set, Set *other) {
  if(set->compare != other->compare) return set_new(void_compare);

  Set *common = set_new(set->compare);
  Iterator *iterator = list_iterator(set->list);
  while(has_next(iterator)) {
    void *value = next(iterator);
    if(set_contains(other, value)) {
      set_managed_add(common, value);
    }
  }

  iterator_destroy(iterator);
  return common;
}

Set *set_difference(Set *set, Set *other) {
  if(set->compare != other->compare) return set_merge(set, other);

  Set *diff = set_new(set->compare);
  Iterator *iterator = list_iterator(set->list);
  while(has_next(iterator)) {
    void *value = next(iterator);
    if(!set_contains(other, value)) {
      set_managed_add(diff, value);
    }
  }

  Iterator *otherIterator = list_iterator(other->list);
  while(has_next(otherIterator)) {
    void *value = next(otherIterator);
    if(!set_contains(set, value)) {
      set_managed_add(diff, value);
    }
  }

  iterator_destroy(iterator);
  iterator_destroy(otherIterator);
  return diff;
}

int set_compare(void *value, void *target) {
  Set *set1 = (Set *)value;
  Set *set2 = (Set *)target;

  return set_equals(set1, set2);
}

Iterator *set_iterator(Set *set) {
  return list_iterator(set->list);
}

Iterator *set_query_iterator(Set *set, QueryFunction query, void *arg) {
  return list_query_iterator(set->list, query, arg);
}

List *set_map(Set *set, void *(*fn) (void *value)) {
  List *results = list_new(ListAny);

  Iterator *iterator = set_iterator(set);
  while(has_next(iterator)) {
    list_add(results, fn( next(iterator) ));
  }

  iterator_destroy(iterator);
  return results;
}

Set *set_map_unique(Set *set, ComparisonFunction compare, void *(*fn) (void *value)) {
  Set *results = set_new(compare);

  Iterator *iterator = set_iterator(set);
  while(has_next(iterator)) {
    set_managed_add(results, fn( next(iterator) ));
  }

  iterator_destroy(iterator);
  return results;
}
