#ifndef __UTILITY_SET__
#define __UTILITY_SET__

#include "types.h"
#include "list.h"

/* Types **********************************************************************/
typedef struct Set {
	List *list;
  ComparisonFunction compare;
} Set;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
Set *set_new(ComparisonFunction compare);
Set *set_newi();
Set *set_newc();
Set *set_newf();
Set *set_newl();
Set *set_newd();
Set *set_news();
Set *set_from_list(List *list, ComparisonFunction compare);
void set_destroy(Set *set);

int set_add(Set *set, void *value);
int set_addi(Set *set, int value);
int set_addc(Set *set, char value);
int set_addf(Set *set, float value);
int set_addl(Set *set, long value);
int set_addd(Set *set, double value);
int set_adds(Set *set, const char *value);
int set_managed_add(Set *set, void *value);

void set_remove(Set *set, void *value);
void set_removei(Set *set, int value);
void set_removec(Set *set, char value);
void set_removef(Set *set, float value);
void set_removel(Set *set, long value);
void set_removed(Set *set, double value);
void set_removes(Set *set, const char *value);

int set_contains(Set *set, void *value);
int set_containsi(Set *set, int value);
int set_containsc(Set *set, char value);
int set_containsf(Set *set, float value);
int set_containsl(Set *set, long value);
int set_containsd(Set *set, double value);
int set_containss(Set *set, const char *value);

int set_size(Set *set);
void set_each(Set *set, void (*fn)(void *value));
int set_equals(Set *set, Set *other);
Set *set_copy(Set* set);
Set *set_merge(Set *set, Set *other);

Set *set_common(Set *set, Set *other);
Set *set_difference(Set *set, Set *other);

int set_compare(void *value, void *target);
Iterator *set_iterator(Set *set);
Iterator *set_query_iterator(Set *set, QueryFunction query, void *arg);

List *set_map(Set *set, void *(*fn) (void *value));
Set *set_map_unique(Set *set, ComparisonFunction compare, void *(*fn) (void *value));

#endif
