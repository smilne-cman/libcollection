#ifndef __UTILITY_ITERATOR__
#define __UTILITY_ITERATOR__

/* Types **********************************************************************/
typedef struct Iterator {
	void *collection;
  void *meta;
  void *(*next)(struct Iterator *iterator);
	int (*hasNext)(struct Iterator *iterator);
} Iterator;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
Iterator *iterator_new(void *collection, void *(next)(Iterator *iterator), int (*hasNext)(struct Iterator *iterator), void *meta);
void iterator_destroy(Iterator *iterator);

void *next(Iterator *iterator);
int has_next(Iterator *iterator);

#endif
