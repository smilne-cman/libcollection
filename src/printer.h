#ifndef __printer__
#define __printer__

/* Includes *******************************************************************/

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
void int_printer(void *value);
void char_printer(void *value);
void float_printer(void *value);
void long_printer(void *value);
void double_printer(void *value);
void string_printer(void *value);

#endif
