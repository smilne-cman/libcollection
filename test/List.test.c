#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include <all.h>

/* Prototypes *****************************************************************/
void before_each();
void after_each();

void test_new();
void test_add();
void test_unmanaged_add();
void test_managed_add();
void test_each();
void test_get();
void test_insert();
void test_remove();
void test_types();
void test_index();
void test_index_of();
void test_index_where();
void test_value_where();
void test_order_by();
void test_order_by_descending();
void test_equals();
void test_contains();
void test_append();
void test_string();
void test_iterator();
void test_copy();
void test_list_compare();
void test_slice();
void test_query_iterator();
void test_join();

void list_iterator_fn(int i, void *value);
int where_iterator(void *actual, void *expected);
List *basic_list();
static int query_starts_with(const char *value, const char *arg);
static int string_starts_with(const char *pre, const char *str);

extern ListItem *list_index(List *list, int index);

/* Global Variables ***********************************************************/
static List *list;
static int counter = 0;

int main(int argc, char *argv[]) {
	test_init("List", argc, argv);
	test_after_each(after_each);

	test("New List", test_new);
	test("Add", test_add);
	test("Unmanaged Add", test_unmanaged_add);
	test("Managed Add", test_managed_add);
	test("Each", test_each);
	test("Get", test_get);
	test("Insert", test_insert);
	test("Remove", test_remove);
	test("Type Safety", test_types);
	test("Index", test_index);
	test("Index Of", test_index_of);
	test("Index Where", test_index_where);
	test("Value Where", test_value_where);
  test("Order By", test_order_by);
  test("Order By Descending", test_order_by_descending);
  test("Equals", test_equals);
  test("Contains", test_contains);
  test("Append", test_append);
	test("String Handling", test_string);
	test("Iterator", test_iterator);
	test("Copy", test_copy);
	test("List Compare", test_list_compare);
	test("Slice", test_slice);
	test("Query Iterator", test_query_iterator);
  test("Join", test_join);

	return test_complete();
}

void after_each() {
	list_destroy(list);
}

void test_new() {
	list = list_new(ListAny);

	test_assert_int( list->length, 0, "Should start with zero length" );
	test_assert_int( list->type, ListAny, "Should have specified type" );
	test_assert( list->start == NULL, "Should not have first element populated" );
	test_assert( list->end == NULL, "Should not have last element populated" );
}

void test_add() {
	list = list_new(ListAny);

	int value = 123;
	list_add(list, &value);

	test_assert_int( list->length, 1, "Should increase the list length");

	ListItem *item = list->start;
	test_assert_int( *(int *)item->value, 123, "Should set specified value in first element");
	test_assert( list->start == list->end, "Should set both the first and last elements");

	list_add(list, &value);
	test_assert_int( list->length, 2, "Should increment the list length again");
	test_assert( list->start->next == list->end, "Should set end to new last element");
}

void test_unmanaged_add() {
	list = list_new(ListAny);

	int value1 = 123;
	int value2 = 456;
	int value3 = 789;

	list_add(list, &value1);
	list_add(list, &value2);
	list_add(list, &value3);

	test_assert_int( *(int *)list->start->value, 123, "Should add first value");
	test_assert_int( *(int *)list->start->next->value, 456, "Should add second value");
	test_assert_int( *(int *)list->start->next->next->value, 789, "Should add third value");

	test_assert( list->start->next->next == list->end, "Should set the last element to the last added value");
}

void test_managed_add() {
	list = list_new(ListAny);
	ListItem *item = NULL;

	list_addi(list, 123);
	list_addc(list, 'c');
	list_addf(list, 0.123f);
	list_addl(list, 123l);
	list_addd(list, 3.141592653589793238l);

	test_assert_int( *(int *)list->start->value, 123, "Should add integer values");
	test_assert_char( *(char *)list->start->next->value, 'c', "Should add character values");
	test_assert_float( *(float *)list->start->next->next->value, 0.123f, "Should add float values");
	test_assert_long( *(long *)list->start->next->next->next->value, 123l, "Should add long values");
	test_assert_double( *(double *)list->start->next->next->next->next->value, 3.141592653589793238l, "Should add double values");
}

void test_each() {
	list = basic_list();

	list_each(list, list_iterator_fn);
	test_assert_int(counter, 3, "Should call iterator for each item in the list");
}

void list_iterator_fn(int i, void *value) {
	test_assert_int( *(int *)value, ++counter, "Should call iterator with correct item value");
}

void test_get() {
	list = basic_list();

	test_assert_int( *(int *)list_get(list, 0), 1, "Should return the first index");
	test_assert_int( *(int *)list_get(list, 1), 2, "Should return the second index");
	test_assert_int( *(int *)list_get(list, 2), 3, "Should return the third index");
  test_assert(list_get(list, 3) == NULL, "Should return NULL when index out of bounds");
}

void test_insert() {
	list = list_new(ListAny);

	list_addi(list, 1);
	list_addi(list, 3);

	int value = 2;
	list_insert(list, 1, &value);

	test_assert_int( list->length, 3, "Should increment the list length");
	test_assert_int( *(int *)list->start->next->value, 2, "Should insert the value at the specified index");
}

void test_remove() {
	list = list_new(ListString);

  list_adds(list, "one");
  list_adds(list, "two");
  list_adds(list, "three");

	list_remove(list, 1);

	test_assert_int( list->length, 2, "Should decrement the list length");
	test_assert_string(list->start->next->value, "three", "Should move the next value to the index");

	list_remove(list, 0);

	test_assert_int( list->length, 1, "Should decrement the list length");
	test_assert_string(list->start->value, "three", "Should delete first item in list");

  list_remove(list, 0);

	test_assert_int(list->length, 0, "Should decrement the list length");
  test_assert(list->start == NULL, "Should set the first list item to NULL");
  test_assert(list->end == NULL, "Should set the last list item to NULL");
}

void test_types() {
	list = list_new(ListInt);

	list_addi(list, 1);
	test_assert_int( list->length, 1, "Should add specified type");

	list_addc(list, 'c');
	list_addf(list, 0.123f);
	list_addl(list, 123l);
	list_addd(list, 3.0);

	test_assert_int( list->length, 1, "Should not add other types");

	int value = 2;
	list_add(list, &value);
	test_assert_int( list->length, 1, "Should not add values with generic add function");
}

int get_from_index(List *list, int index) {
	ListItem *item = list_index(list, index);

	return *(int *)item->value;
}

void test_index() {
	list = basic_list();

	test_assert_int(get_from_index(list, 0), 1, "Should retrieve the first list item");
	test_assert_int(get_from_index(list, 1), 2, "Should retrieve the middle list item");
	test_assert_int(get_from_index(list, 2), 3, "Should retrieve the last list item");

	test_assert_int(get_from_index(list, -1), 3, "Should retrieve from the end of the list when a negative index is given");

	test_assert(list_index(list, 3) == NULL, "Should return null for positive out of bounds call");
	test_assert(list_index(list, -4) == NULL, "Should return null for negative out of bounds call");
}

void test_index_of() {
	list = list_new(ListAny);

	int value1 = 123;
	int value2 = 456;
	int value3 = 789;
	int value4 = 987;

	list_add(list, &value1);
	list_add(list, &value2);
	list_add(list, &value3);

	test_assert_int( list_index_of(list, &value2), 1, "Should return the correct index");
	test_assert_int( list_index_of(list, &value4), -1, "Should return -1 if item not found");
}

void test_index_where() {
	list = list_new(ListAny);

	int value1 = 123;
	int value2 = 456;
	int value3 = 789;
	int value4 = 987;
	int other2 = 456;

	list_add(list, &value1);
	list_add(list, &value2);
	list_add(list, &value3);

	test_assert_int( list_index_where(list, &other2, int_compare), 1, "Should return the index when the comparator returns true");
	test_assert_int( list_index_where(list, &value4, int_compare), -1, "Should return -1 if comparator never returns true");
}

void test_value_where() {
	list = list_new(ListAny);

	int value1 = 123;
	int value2 = 456;
	int value3 = 789;
	int value4 = 987;
	int other2 = 456;

	list_add(list, &value1);
	list_add(list, &value2);
	list_add(list, &value3);

	test_assert_int( *(int *)list_value_where(list, &other2, int_compare), 456, "Should return the value when the comparator returns true");
	test_assert( list_value_where(list, &value4, int_compare) == NULL, "Should return NULL if comparator never returns true");
}

void test_order_by() {
  list = list_new(ListInt);

  // Add numbers out of order, this should take three passes to sort
  list_addi(list, 4); // 1 1 1
  list_addi(list, 7); // 4 2 2
  list_addi(list, 2); // 7 4 4
  list_addi(list, 1); // 2 7 5
  list_addi(list, 5); // 5 5 7

  list_order_by(list, int_compare, SortAscending);

  test_assert_int( list_geti(list, 0), 1, "Should sort the lowest value to the start of the list");
  test_assert_int( list_geti(list, 1), 2, "Should sort the second entry correctly");
  test_assert_int( list_geti(list, 2), 4, "Should sort the second third correctly");
  test_assert_int( list_geti(list, 3), 5, "Should sort the second fourth correctly");
  test_assert_int( list_geti(list, 4), 7, "Should sort the highest value to the end of the list");
}

void test_order_by_descending() {
  list = list_new(ListInt);

  // Add numbers out of order, this should take two passes to sort
  list_addi(list, 4); // 7 7
  list_addi(list, 7); // 4 5
  list_addi(list, 2); // 5 4
  list_addi(list, 1); // 2 2
  list_addi(list, 5); // 1 1

  list_order_by(list, int_compare, SortDescending);

  test_assert_int( list_geti(list, 0), 7, "Should sort the highest value to the start of the list");
  test_assert_int( list_geti(list, 1), 5, "Should sort the second entry correctly");
  test_assert_int( list_geti(list, 2), 4, "Should sort the third entry correctly");
  test_assert_int( list_geti(list, 3), 2, "Should sort the fourth entry correctly");
  test_assert_int( list_geti(list, 4), 1, "Should sort the lowest value to the end of the list");
}

void test_equals() {
  list = list_new(ListInt);
  List *other = list_new(ListInt);
  List *anyList = list_new(ListAny);
  List *differentLength = list_new(ListInt);

  list_addi(list, 1);
  list_addi(list, 2);
  list_addi(list, 3);

  list_addi(other, 1);
  list_addi(other, 2);
  list_addi(other, 3);

  list_addi(anyList, 1);
  list_addi(anyList, 2);
  list_addi(anyList, 3);

  list_addi(differentLength, 1);
  list_addi(differentLength, 2);

  test_assert( list_equals(list, other, int_compare), "Two identical lists should be equal");
  test_assert( !list_equals(list, anyList, int_compare), "Lists with different types should not be equal");
  test_assert( !list_equals(list, differentLength, int_compare), "Lists with different lengths should not be equal");

  list_destroy(other);
  list_destroy(anyList);
  list_destroy(differentLength);
}

void test_contains() {
  list = list_new(ListInt);

  list_addi(list, 27);
  list_addi(list, 123);
  list_addi(list, 42);

  int value = 42;
  int value2 = 124;
  test_assert( list_contains(list, &value, int_compare), "Should return true when the list contains the item");
  test_assert( !list_contains(list, &value2, int_compare), "Should return false when the list does not contain the item");
}

void test_append() {
  list = list_new(ListInt);
  List *source = list_new(ListInt);

  list_addi(list, 1);
	list_addi(list, 2);

  list_addi(source, 3);
	list_addi(source, 4);

  list_append(list, source);

  test_assert_int(list->length, 4, "Should increment the length of the target list");
  test_assert_int(list_geti(list, 2), 3, "Should add first value from source list");
  test_assert_int(list_geti(list, 3), 4, "Should add second value from source list");

  list_destroy(source);
}

void test_string() {
	list = list_new(ListString);

	list_adds(list, "Hello world!");
	list_adds(list, "ABC");
	list_adds(list, "123");

	test_assert_int(list->length, 3, "Should add three strings to the list");
	test_assert_int(list_index_ofs(list, "123"), 2, "Should be able to get index by value");

	test_assert_string(list_gets(list, 0), "Hello world!", "Should store and return first string");
	test_assert_string(list_gets(list, 1), "ABC", "Should store and return second string");
	test_assert_string(list_gets(list, 2), "123", "Should store and return third string");
}

void test_iterator() {
	list = basic_list();
	Iterator *iterator = list_iterator(list);

	int expected = 1;
	while(has_next(iterator)) {
		int value = *(int *)next(iterator);
		test_assert_int(value, expected, "Should iterate over each value");

		expected++;
	}

	test_assert_int(expected, 4, "Should iterate three times");
	test_assert(has_next(iterator) == FALSE, "Should return FALSE when iterator is finished");

	iterator_destroy(iterator);
}

void test_copy() {
	list = basic_list();
	List *copy = list_copy(list);

	test_assert_int(list->length, 3, "Should create a list of the same length");
	test_assert_int(list_geti(copy, 0), 1, "Should copy the first element");
	test_assert_int(list_geti(copy, 1), 2, "Should copy the second element");
	test_assert_int(list_geti(copy, 2), 3, "Should copy the third element");

	list_destroy(copy);
}

void test_list_compare() {
	list = list_new(ListInt);
  List *other = list_new(ListInt);
  List *anyList = list_new(ListAny);
  List *differentLength = list_new(ListInt);

  list_addi(list, 1);
  list_addi(list, 2);
  list_addi(list, 3);

  list_addi(other, 1);
  list_addi(other, 2);
  list_addi(other, 3);

  list_addi(anyList, 1);
  list_addi(anyList, 2);
  list_addi(anyList, 3);

  list_addi(differentLength, 1);
  list_addi(differentLength, 2);

  test_assert( list_compare(list, other), "Two equal lists should be equal");
  test_assert( !list_compare(list, anyList), "Lists with different types should not be equal");
  test_assert( !list_compare(list, differentLength), "Lists with different lengths should not be equal");

  list_destroy(other);
  list_destroy(anyList);
  list_destroy(differentLength);
}

void test_slice() {
	list = list_new(ListInt);

	list_addi(list, 1);
	list_addi(list, 2);
	list_addi(list, 3);
	list_addi(list, 4);
	list_addi(list, 5);

	List *slice = list_slice(list, 1, 3);

	test_assert_int(list_size(slice), 3, "Should return a list with three items");
	test_assert_int(list_geti(slice, 0), 2, "Should have correct first entry");
	test_assert_int(list_geti(slice, 1), 3, "Should have correct second entry");
	test_assert_int(list_geti(slice, 2), 4, "Should have correct third entry");

	list_destroy(slice);
}

void test_query_iterator() {
	list = list_new(ListString);

	list_adds(list, "author");
	list_adds(list, "library.libcommand");
	list_adds(list, "library.libstring");

	int iteration = 0;
  Iterator *iterator = list_query_iterator(list, query_starts_with, "library.");
	while(has_next(iterator)) {
    const char *value = (const char *)next(iterator);
		iteration++;
	}

	test_assert_int(iteration, 2, "Should return two results");

	iterator_destroy(iterator);
}

void test_join() {
  list = list_new(ListString);

  list_adds(list, "Item 1");
  list_adds(list, "Item 2");
  list_adds(list, "Item 3");
  test_assert_string( list_join(list, ", ", 64), "Item 1, Item 2, Item 3", "Should join list items with separators");

  List *other = list_new(ListAny);
  list_adds(other, "Item 1");
  list_adds(other, "Item 2");
  list_adds(other, "Item 3");
  test_assert_string( list_join(other, ", ", 64), "", "Should return an empty string with list type is not string");
}

int where_iterator(void *actual, void *expected) {
	int a = *(int *)actual;
	int b = *(int *)expected;

	return a == b;
}

List *basic_list() {
	list = list_new(ListAny);

	list_addi(list, 1);
	list_addi(list, 2);
	list_addi(list, 3);

	return list;
}

static int query_starts_with(const char *value, const char *arg) {
  return string_starts_with(arg, value);
}

static int string_starts_with(const char *pre, const char *str) {
  size_t lenpre = strlen(pre);
  size_t lenstr = strlen(str);

  return lenstr < lenpre ? 0 : memcmp(pre, str, lenpre) == 0;
}
