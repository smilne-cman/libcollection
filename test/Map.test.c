#include <stdio.h>
#include <string.h>
#include <libtest/libtest.h>
#include <all.h>

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void test_new();
void test_put();
void test_get();
void test_remove();
void test_contains();
void test_contains_value();
void test_equals();
void test_copy();
void test_key_set();
void test_value_list();
void test_iterator();
void test_query_iterator();
void test_mutating_keys();

static Map *map_int_by_string();
static Map *map_string_by_string();
static int query_starts_with(MapEntry *value, char *arg);
static int string_starts_with(const char *pre, const char *str);

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
	test_init("Map", argc, argv);

	test("New", test_new);
	test("Put", test_put);
	test("Get", test_get);
	test("Remove", test_remove);
	test("Contains", test_contains);
	test("Contains Value", test_contains_value);
	test("Equals", test_equals);
	test("Copy", test_copy);
	test("Key Set", test_key_set);
	test("Value List", test_value_list);
	test("Iterator", test_iterator);
  test("Query Iterator", test_query_iterator);
  test("Mutating Keys", test_mutating_keys);

	return test_complete();
}

void test_new() {
	Map *map = map_int_by_string();

	test_assert_int(map_size(map), 0, "Should be initialised with no entries");

	map_destroy(map);
}

static Map *map_int_by_string() {
	return map_new(map_by_string(int_compare));
}

static Map *map_string_by_string() {
  return map_new(map_by_string(string_compare));
}

void test_put() {
	Map *map = map_int_by_string();

	int one = 1;
	int two = 2;
	int three = 3;
	int four = 4;

	test_assert(map_put(map, "1", &one), "Should return true when adding new entries");
	map_put(map, "2", &two);
	map_put(map, "3", &three);

	test_assert(map_put(map, "1", &four) == FALSE, "Should return false when value is updated");

	test_assert_int(map_size(map), 3, "Should have three entries");

	map_destroy(map);
}

void test_get() {
	Map *map = map_int_by_string();

	int one = 1;
	int two = 2;
	int three = 3;

	map_put(map, "1", &one);
	map_put(map, "2", &two);
	map_put(map, "1", &three);

	int *value1 = (int *)map_get(map, "1");
	int *value2 = (int *)map_get(map, "2");
	test_assert_int(*value1, 3, "Should update first value");
	test_assert_int(*value2, 2, "Should return second value");

	map_destroy(map);
}

void test_remove() {
	Map *map = map_string_by_string();

  const char *one = "one";
  const char *two = "two";
  const char *three = "three";

	map_put(map, "1", "one");
	map_put(map, "2", "two");
	map_put(map, "3", "three");

	map_remove(map, "2");

	test_assert_int(map_size(map), 2, "Should remove one entry");
  test_assert_string(map_get(map, "1"), one, "Should still contain first entry");
  test_assert_string(map_get(map, "3"), three, "Should still contain last entry");

	map_destroy(map);
}

void test_contains() {
	Map *map = map_string_by_string();

	map_put(map, "1", "one");
	map_put(map, "2", "two");
	map_put(map, "3", "three");

	test_assert(map_contains(map, "2"), "Should contain existing key");
	test_assert(map_contains(map, "4") == FALSE, "Should not contain non-existing key");

	map_destroy(map);
}

void test_contains_value() {
	Map *map = map_int_by_string();

	int one = 1;
	int two = 2;
	int three = 3;
	int four = 4;

	map_put(map, "1", &one);
	map_put(map, "2", &two);
	map_put(map, "3", &three);

	test_assert(map_contains_value(map, &two), "Should contain existing value");
	test_assert(map_contains_value(map, &four) == FALSE, "Should not contain non-existing value");

	map_destroy(map);
}

void test_equals() {
	Map *map1 = map_int_by_string();
	Map *map2 = map_int_by_string();
	Map *map3 = map_int_by_string();
	Map *map4 = map_new(map_type_new(string_compare, float_compare));

	int one = 1;
	int two = 2;
	int three = 3;

	map_put(map1, "1", &one);
	map_put(map1, "2", &two);
	map_put(map1, "3", &three);

	map_put(map2, "3", &three);
	map_put(map2, "2", &two);
	map_put(map2, "1", &one);

	map_put(map3, "1", &three);
	map_put(map3, "2", &two);
	map_put(map3, "3", &one);

	map_put(map4, "1", &one);
	map_put(map4, "2", &two);
	map_put(map4, "3", &three);

	test_assert(map_equals(map1, map2), "Should return TRUE when maps are equal");
	test_assert(map_equals(map1, map3) == FALSE, "Should return FALSE when maps have different values");
	test_assert(map_equals(map1, map4) == FALSE, "Should return FALSE when maps have different types");

	map_destroy(map1);
	map_destroy(map2);
	map_destroy(map3);
	map_destroy(map4);
}

void test_copy() {
	Map *map1 = map_int_by_string();

	int one = 1;
	int two = 2;
	int three = 3;

	map_put(map1, "1", &one);
	map_put(map1, "2", &two);
	map_put(map1, "3", &three);

	Map *map2 = map_copy(map1);

	test_assert(map_equals(map1, map2), "Should return a map that is equal to the initial map");

	map_destroy(map1);
	map_destroy(map2);
}

void test_key_set() {
	Map *map = map_int_by_string();

	int one = 1;
	int two = 2;
	int three = 3;

	map_put(map, "1", &one);
	map_put(map, "2", &two);
	map_put(map, "3", &three);

	Set *keys = map_key_set(map);

	test_assert_int(set_size(keys), 3, "Should return a set with the same size as the map");
	test_assert(set_contains(keys, "1"), "Should contain the first key");
	test_assert(set_contains(keys, "2"), "Should contain the second key");
	test_assert(set_contains(keys, "3"), "Should contain the third key");

	set_destroy(keys);
	map_destroy(map);
}

void test_value_list() {
	Map *map = map_int_by_string();

	int one = 1;
	int two = 2;
	int three = 3;

	map_put(map, "1", &one);
	map_put(map, "2", &two);
	map_put(map, "3", &three);

	List *values = map_value_list(map);

	test_assert_int(values->length, 3, "Should return a list with the same size as the map");

	list_destroy(values);
	map_destroy(map);
}

void test_iterator() {
	Map *map = map_int_by_string();

	int one = 1;
	int two = 2;
	int three = 3;

	map_put(map, "1", &one);
	map_put(map, "2", &two);
	map_put(map, "3", &three);

	int iterations = 0;
	Iterator *iterator = map_iterator(map);
	while(has_next(iterator)) {
		iterations++;

		MapEntry *entry = next_map(iterator);
		test_assert(map_contains(map, entry->key), "Should return a map entry key from the map");
		test_assert(map_contains_value(map, entry->value), "Should return a map entry value from the map");
	}

	test_assert_int(iterations, 3, "Should iterate once for each map entry");

	iterator_destroy(iterator);
	map_destroy(map);
}

void test_query_iterator() {
  Map *map = map_string_by_string();

  map_put(map, "author", "Steven I Milne (stevenimilne@outlook.com)");
  map_put(map, "library.libcommand", "1.0.1");
  map_put(map, "library.libstring", "1.0.1");

  int iterations = 0;
  Iterator *iterator = map_query_iterator(map, query_starts_with, "library.");
  while(has_next(iterator)) {
    MapEntry *entry = (MapEntry *)next(iterator);
    iterations++;
  }

  test_assert_int(iterations, 2, "Should iterate once for each matching map entry");

  iterator_destroy(iterator);
  map_destroy(map);
}

void test_mutating_keys() {
  Map *map = map_int_by_string();
  char *key = (char *)malloc(sizeof(char) * 20);

  strcpy(key, "one");
  map_put(map, key, 1);

  strcpy(key, "two");
  map_put(map, key, 2);

  strcpy(key, "three");
  map_put(map, key, 3);

  test_assert_int( map_get(map, "one"), 1, "Should clone first key" );
  test_assert_int( map_get(map, "two"), 2, "Should clone second key" );
  test_assert_int( map_get(map, "three"), 3, "Should clone third key" );
}

static int query_starts_with(MapEntry *value, char *arg) {
  return string_starts_with(arg, value->key);
}

static int string_starts_with(const char *pre, const char *str) {
  size_t lenpre = strlen(pre);
  size_t lenstr = strlen(str);

  return lenstr < lenpre ? 0 : memcmp(pre, str, lenpre) == 0;
}
