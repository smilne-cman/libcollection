#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include <all.h>

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void test_add();
void test_contains();
void test_remove();
void test_equals();
void test_copy();
void test_merge();
void test_common();
void test_difference();
void test_iterator();
void test_query_iterator();
void test_type_safety();
void test_map();
void test_from_list();
void test_map_unique();

void *mapping_fn(void *value);
void *mapping_unique_fn(void *value);
static int query_starts_with(const char *value, const char *arg);
static int string_starts_with(const char *pre, const char *str);

/* Global Variables ***********************************************************/
static int trueValue = TRUE;
static int falseValue = FALSE;

/* Functions ******************************************************************/
int main(int argc, char *argv[]) {
	test_init("Set", argc, argv);

	test("Add", test_add);
	test("Contains", test_contains);
  test("Remove", test_remove);
  test("Equals", test_equals);
  test("Copy", test_copy);
  test("Merge", test_merge);
  test("Common", test_common);
  test("Difference", test_difference);
  test("Iterator", test_iterator);
  test("Query Iterator", test_query_iterator);
  test("Type Safety", test_type_safety);
	test("Map", test_map);
	test("From List", test_from_list);
	test("Map Unique", test_map_unique);

	return test_complete();
}

/* Tests **********************************************************************/
void test_add() {
	Set *set = set_newi();

	test_assert(set_addi(set, 1), "Should return true when the value is added");
	test_assert(set_addi(set, 1) == FALSE, "Should return false when the value is not added");

	test_assert_int(set_size(set), 1, "Should only add the value once");

	set_destroy(set);
}

void test_contains() {
	Set *set = set_newi();

  set_addi(set, 1);
  set_addi(set, 2);
  set_addi(set, 3);

  test_assert_int(set_size(set), 3, "Should add three items to the set");
  test_assert(set_containsi(set, 1), "Should contain first added item");
  test_assert(set_containsi(set, 2), "Should contain second added item");
  test_assert(set_containsi(set, 3), "Should contain third added item");
  test_assert(set_containsi(set, 4) == FALSE, "Should not contain fourth value");

  set_destroy(set);
}

void test_remove() {
  Set *set = set_news();

  set_adds(set, "one");
  set_adds(set, "two");
  set_adds(set, "three");

  test_assert_int(set_size(set), 3, "SANITY -- Three items should be added");
  set_removes(set, "two");

  test_assert_int(set_size(set), 2, "Should reduce size of set");
  test_assert(set_containss(set, "one"), "Should still contain first added item");
  test_assert(set_containss(set, "two") == FALSE, "Should remove second item");
  test_assert(set_containss(set, "three"), "Should still contain third added item");

  set_destroy(set);
}

void test_equals() {
  Set *set1 = set_newi();
  Set *set2 = set_newi();
  Set *set3 = set_newi();

  set_addi(set1, 1);
  set_addi(set1, 2);
  set_addi(set1, 3);

  set_addi(set2, 3);
  set_addi(set2, 2);
  set_addi(set2, 1);

  set_addi(set3, 8);
  set_addi(set3, 2);

  test_assert(set_equals(set1, set2), "Set 1 and 2 should be equal");
  test_assert(set_equals(set1, set3) == FALSE, "Set 1 and 3 should not be equal");

  set_destroy(set1);
  set_destroy(set2);
  set_destroy(set3);
}

void test_copy() {
  Set *set = set_newi();

  set_addi(set, 1);
  set_addi(set, 2);
  set_addi(set, 3);

  Set *copy = set_copy(set);

  test_assert_int(set_size(copy), 3, "Should return a set with the same number of elements");
  test_assert(set_equals(set, copy), "Should create an set which satisfies 'set_equals'");

  set_destroy(set);
  set_destroy(copy);
}

void test_merge() {
  Set *set1 = set_newi();
  Set *set2 = set_newi();

  set_addi(set1, 1);
  set_addi(set1, 2);
  set_addi(set1, 3);

  set_addi(set2, 4);
  set_addi(set2, 5);
  set_addi(set2, 6);

  Set *merged = set_merge(set1, set2);

  test_assert_int(set_size(merged), 6, "Should return a set with the combined size of both sets");
  test_assert(merged->compare == set1->compare, "Should return a set with the same compare function");
  test_assert(set_containsi(merged, 1), "Should contain elements from the first set");
  test_assert(set_containsi(merged, 2), "Should contain elements from the first set");
  test_assert(set_containsi(merged, 3), "Should contain elements from the first set");
  test_assert(set_containsi(merged, 4), "Should contain elements from the second set");
  test_assert(set_containsi(merged, 5), "Should contain elements from the second set");
  test_assert(set_containsi(merged, 6), "Should contain elements from the second set");

  set_destroy(set1);
  set_destroy(set2);
  set_destroy(merged);
}

void test_common() {
  Set *set1 = set_newi();
  Set *set2 = set_newi();

  set_addi(set1, 1);
  set_addi(set1, 2);
  set_addi(set1, 3);

  set_addi(set2, 2);
  set_addi(set2, 3);
  set_addi(set2, 4);

  Set *common = set_common(set1, set2);
  test_assert_int(set_size(common), 2, "Should return a set with two elements");
  test_assert(set_containsi(common, 2), "Should contain first element in common");
  test_assert(set_containsi(common, 3), "Should contain second element in common");

  set_destroy(set1);
  set_destroy(set2);
  set_destroy(common);
}

void test_difference() {
  Set *set1 = set_newi();
  Set *set2 = set_newi();

  set_addi(set1, 1);
  set_addi(set1, 2);
  set_addi(set1, 3);

  set_addi(set2, 2);
  set_addi(set2, 3);
  set_addi(set2, 4);

  Set *diff = set_difference(set1, set2);
  test_assert_int(set_size(diff), 2, "Should return a set with two elements");
  test_assert(set_containsi(diff, 1), "Should contain first element not found in both sets");
  test_assert(set_containsi(diff, 4), "Should contain second element not found in both sets");

  set_destroy(set1);
  set_destroy(set2);
  set_destroy(diff);
}

void test_iterator() {
  Set *set = set_newi();

  set_addi(set, 1);
  set_addi(set, 2);
  set_addi(set, 3);

  int iterations = 0;
  int total = 0;

  Iterator *iterator = set_iterator(set);
  while(has_next(iterator)) {
    int *pointer = (int *)next(iterator);
    iterations++;
    total += *pointer;
  }

  test_assert_int(iterations, 3, "Should iterate once for each value");
  test_assert_int(total, 6, "Should provide each value");

  iterator_destroy(iterator);
  set_destroy(set);
}

void test_query_iterator() {
  Set *set = set_news();

  set_adds(set, "author");
  set_adds(set, "library.libcommand");
  set_adds(set, "library.libstring");

  int iterations = 0;
  Iterator *iterator = set_query_iterator(set, query_starts_with, "library.");
  while(has_next(iterator)) {
    const char *pointer = (const char *)next(iterator);
    iterations++;
  }

  test_assert_int(iterations, 2, "Should iterate once for each matching value");

  iterator_destroy(iterator);
  set_destroy(set);
}

void test_type_safety() {
  Set *set = set_newi();

  int value = 1;
  set_add(set, &value);
  set_addi(set, 2);
  set_addc(set, 'a');
  set_adds(set, "Hello world!");

  test_assert_int(set_size(set), 1, "Should not add elements of the wrong type to the set");
  test_assert(set_containsi(set, 2), "Should only add elements to the set when the correct type safe add function is called");
}

void test_map() {
	Set *set = set_newi();

	set_addi(set, 1);
	set_addi(set, 2);
	set_addi(set, 3);

	List *results = set_map(set, mapping_fn);

	test_assert_int(results->length, 3, "Should return a list with an item for each set element");

	list_destroy(results);
	set_destroy(set);
}

void *mapping_fn(void *value) {
	return value;
}

void test_from_list() {
	List *list = list_new(ListInt);

	list_addi(list, 1);
	list_addi(list, 2);
	list_addi(list, 3);
	list_addi(list, 2);

	Set *set = set_from_list(list, int_compare);

	test_assert_int(set_size(set), 3, "Should create a set with three elements, pruning duplicates");
	test_assert(set_containsi(set, 1), "Should contain first list element");
	test_assert(set_containsi(set, 2), "Should contain second list element");
	test_assert(set_containsi(set, 3), "Should contain third list element");

	set_destroy(set);
}

void test_map_unique() {
	Set *set1 = set_newi();

	set_addi(set1, 1);
	set_addi(set1, 2);
	set_addi(set1, 3);

	Set *set2 = set_map_unique(set1, int_compare, mapping_unique_fn);

	test_assert_int(set_size(set2), 2, "Should only return unique values");
	test_assert(set_containsi(set2, trueValue), "Should contain first value");
	test_assert(set_containsi(set2, falseValue), "Should contain second value");

	set_destroy(set1);
	set_destroy(set2);
}

void *mapping_unique_fn(void *value) {
	int *intValue = (int *)value;

	if(*intValue > 1) {
		return &falseValue;
	}

	return &trueValue;
}

static int query_starts_with(const char *value, const char *arg) {
  return string_starts_with(arg, value);
}

static int string_starts_with(const char *pre, const char *str) {
  size_t lenpre = strlen(pre);
  size_t lenstr = strlen(str);

  return lenstr < lenpre ? 0 : memcmp(pre, str, lenpre) == 0;
}
